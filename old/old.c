#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

//#define BUFFER 5000

size_t	ft_strlen(const char *str)
{
	size_t	result;

	result = 0;
	while (str[result] != '\0')
	{
		result++;
	}
	return (result);
}

void ft_memcpy(void *dest, void *src, int n){
  char* tdest = (char *)dest;
  char* tsrc = (char *)src;

  for(int i=0; i < n; i++)
    tdest[i] = tsrc[i];
}

int ft_inputlen(int fd){
  int bytes_read;
  int count = 0;
  do{
    char c = 0;
    bytes_read = read(fd, &c, 1);
    count++;
  }while(bytes_read != 0);

  return count;
}

/*
char *get_content(int fd, char * result){
	char *buffer = malloc(BUFFER * sizeof(char));
	int rd = read(fd, buffer, BUFFER);
  for(int i = 0; i < rd; i++){
    printf("%c", buffer[i]);
  }  
  ft_memcpy(result, buffer, rd);

	free(buffer);
	buffer = NULL;
	
	return result;
}
*/

int ft_nlen(char *str){
  int count = 0;

  while(str[count] != '\0' && str[count] != '\n'){
    count++;
  }

  return ++count;
}

char * ft_realloc(char * str){
	int i;

	i = 0;
	while(str[i] != '\n' && str[i])
		i++;

	if(!str[i]){
		free(str);
		return NULL;
	}
	int len = ft_strlen(str) - i + 1;
	char * tmp = malloc(sizeof(char) * (ft_strlen(str) - i + 1));
	if(!tmp)
		return NULL;
	ft_memcpy(tmp, str+i, len);
	tmp[len] = '\0';
	free(str);
	return tmp;
}


char * get_next_line(int fd){
	if(!fd || BUFFER_SIZE <= 0)
		return NULL;

	static char * content;
  if(!content){
  char * buffer = malloc(sizeof(char) * BUFFER_SIZE);
	if(!buffer)
		return NULL;

  int rd = read(fd, buffer, BUFFER_SIZE);
	if(rd < 0)
		return NULL;
  int copy = rd;
  if(rd > BUFFER_SIZE)
    copy = BUFFER_SIZE;
  //printf("copy: %d\n", copy);
  content = malloc(sizeof(char) * copy);
  if(!content)
		return NULL;
	for(int i=0; i< copy; i++){
    content[i] = '\0';
  }
  ft_memcpy(content, buffer, copy);
	free(buffer);
  }
  /*
  printf("\n////////File size/////////\n");
  printf("%d\n", rd);
  printf("\n///////File content////////\n");
  printf("%s\n", content); 
  printf("////////////////////////////\n"); 
  //}*/
  //printf("%s\n", content);
  int trim = ft_nlen(content);
  //printf("trim: %d\n", trim);
  char * result = malloc(sizeof(char) * (trim));
  if(!result)
		return NULL;

	result[trim] = '\0';
  ft_memcpy(result, content, trim-1);
	content = ft_realloc(content);
  //printf("%s\n", result);
  //if(!content && ft_strlen(result) == 0{
  //  result = NULL;
	//}
	return result;
}
int main(void){
	int file;
	file = open("input.txt", O_RDONLY);
  //file = open("../svcnk.html", O_RDONLY);
  char *str;
  int i = 1;
	do{
    str =  get_next_line(file);
	  printf("%2d line: |%s|\n", i++, str);
  }while(str != NULL);
	close(file);

	return 0;
}
