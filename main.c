#include "get_next_line.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

// gcc -Wall -Wextra -Werror -D BUFFER_SIZE=42 get_next_line.c get_next_line.h

int main(void){
	int file;
	file = open("input.txt", O_RDONLY);
  //file = open("../svcnk.html", O_RDONLY);
  char *str;
  int i = 1;
	do{
    str =  get_next_line(file);
	  printf("%2d line: |%s", i++, str);
		free(str);
  }while(str != NULL);
	close(file);
	
	return 0;
}
