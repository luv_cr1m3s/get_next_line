#!/bin/bash
gcc -Wall -Wextra -Werror -D BUFFER_SIZE=$1 main.c get_next_line.h get_next_line_utils.c get_next_line.c -o get_next_line #get_next_line.h 

sleep 1
valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --verbose --log-file=valgrind-out.txt ./get_next_line
